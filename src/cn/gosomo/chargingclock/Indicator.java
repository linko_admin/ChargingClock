
package cn.gosomo.chargingclock;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import cn.gosomo.chargingclock.R;

public class Indicator extends View {
    private Drawable mNormalIndicator;
    private Drawable mSelectedIndicator;
    private int mIndicatorCount = 4;
    private int mScale = 15;
    private int selectedIndex = 0;
    private boolean mIsAllSelected = false;

    public Indicator(Context context) {
        super(context);
    }

    public Indicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public Indicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.IndicatorView);
        mNormalIndicator = (Drawable) a
                .getDrawable(R.styleable.IndicatorView_normal_indicator);
        mSelectedIndicator = (Drawable) a
                .getDrawable(R.styleable.IndicatorView_seleted_indicator);
        a.recycle();
    }

    public int getIndicatorCount() {
        return mIndicatorCount;
    }

    public void setIndicatorCount(int count) {
        mIndicatorCount = count;
        invalidate();
    }

    public void setSelectedIndicator(int index) {
        selectedIndex = index;
        invalidate();
    }

    public void setAllSelected(boolean selected){
        mIsAllSelected = selected;
    }

    @Override
    public void draw(Canvas canvas) {
        int width = getWidth();
        int hight = getLayoutParams().height;
        int imgHeight = mNormalIndicator.getIntrinsicHeight();
        int drawImgHight = imgHeight;
        if (imgHeight > hight) {
            drawImgHight = hight;
        }
        int totalWidth = mIndicatorCount * drawImgHight + (mIndicatorCount - 1) * mScale;
        Drawable d = null;
        for (int i = 0; i < mIndicatorCount; i++) {
            int startDrwPosition = (width - totalWidth) / 2;
            if (selectedIndex == i) {
                d = mSelectedIndicator;
            } else {
                d = mIsAllSelected ? mSelectedIndicator : mNormalIndicator;
            }
            startDrwPosition = startDrwPosition + i * (drawImgHight + mScale);
            d.setBounds(startDrwPosition, 0, startDrwPosition + drawImgHight, drawImgHight);
            d.draw(canvas);
        }
    }

}
