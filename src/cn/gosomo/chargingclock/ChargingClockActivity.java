
package cn.gosomo.chargingclock;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextClock;
import android.widget.TextView;

public class ChargingClockActivity extends Activity {
    private static Activity mActivity;
    private TextClock mDigitalClock;
    private Indicator mIndicator;
    private final int mIndicatorCount = 8;
    private PowerManager.WakeLock mWakeLock;
    private final int mFlags = (WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
            | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
            | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    private final int COMMAND_CHANGE = 0;
    private HomeKeyReceiver mHomeKeyReceiver;
    private SensorManager mSensorManager;
    private Sensor mLigthSensor;
    private LigthSensorListener mLigthSensorListener;
    public final int LIGHT_SENSOR_RATE = 0xA8 * 1000;
    private final int LIGHT_0 = 150;
    private final int LIGHT_1 = 600;
    private final float ALPHA_0 = 0.4F;
    private final float ALPHA_1 = 1.0F;
    private TextView mDate;
    private ScreensaverMoveSaverRunnable mMoveSaverRunnable;
    private BatteryBroadcastReceiver BatteryBroadcastReceiver;
    private View mContentView;
    private boolean mIsShowIndicator;
    private boolean mIsStartMoveSaverRunnable = false;
    private Handler mMoveHandler = new Handler();
    private Handler mHandler = new Handler() {
        int count = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case COMMAND_CHANGE:
                    count++;
                    if (count == mIndicatorCount) {
                        count = 0;
                    }
                    mIndicator.setSelectedIndicator(count);
                    mHandler.removeMessages(COMMAND_CHANGE);
                    mHandler.sendEmptyMessageDelayed(COMMAND_CHANGE, 300);
                    break;

                default:
                    break;
            }
        }

    };

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        finishActivity();
        return true;
    }

    @Override
    public boolean dispatchKeyShortcutEvent(KeyEvent event) {
        finishActivity();
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        finishActivity();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;

        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        winParams.flags |= mFlags;
        setContentView(R.layout.activity_main);
        mContentView = findViewById(R.id.main_layout);
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);

        mIsShowIndicator = getResources().getBoolean(R.bool.config_show_indicator);
        mIndicator = (Indicator) findViewById(R.id.charging_indicator);
        mIndicator.setIndicatorCount(mIndicatorCount);
        mIndicator.setVisibility(mIsShowIndicator ? View.VISIBLE : View.GONE);

        mDigitalClock = (TextClock) findViewById(R.id.digital_clock);
        setTimeFormat(mDigitalClock,
                (int) getResources().getDimension(R.dimen.main_ampm_font_size));
        mDate = (TextView) findViewById(R.id.date);
        updateDate(mDate);

        mHandler.sendEmptyMessageDelayed(COMMAND_CHANGE, 1000);
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.SCREEN_DIM_WAKE_LOCK, "bright");
        mWakeLock.acquire();

        registerHomeKeyListener();

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mLigthSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mLigthSensorListener = new LigthSensorListener();
        mSensorManager.registerListener(mLigthSensorListener, mLigthSensor, LIGHT_SENSOR_RATE);

        registerBatteryReceiver();

        View saverView = mContentView.findViewById(R.id.saver_view);
        saverView.setAlpha(0);
        mMoveSaverRunnable.registerViews(mContentView, saverView);
    }

    public ChargingClockActivity() {
        mMoveSaverRunnable = new ScreensaverMoveSaverRunnable(mMoveHandler);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsStartMoveSaverRunnable){
            mIsStartMoveSaverRunnable = mMoveHandler.post(mMoveSaverRunnable);
        }
    }

    private void registerBatteryReceiver() {
        BatteryBroadcastReceiver = new BatteryBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(BatteryBroadcastReceiver, filter);
    }

    private class BatteryBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
                    BatteryManager.BATTERY_STATUS_UNKNOWN);
            if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
                mIndicator.setAllSelected(false);
            } else if (status == BatteryManager.BATTERY_STATUS_FULL) {
                mIndicator.setAllSelected(true);
            }
        }

    }

    public class LigthSensorListener implements SensorEventListener {

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            float value = event.values[0];
            float alpha = ALPHA_1;
            if (value < LIGHT_0) {
                alpha = ALPHA_0;
            } else if (value < LIGHT_1) {
                alpha = ALPHA_1;
            }

            updateViewAlpha(alpha);
        }

    }

    private void updateViewAlpha(float alpha) {
        ObjectAnimator.ofFloat(mDate, "alpha", alpha)
                .setDuration(500)
                .start();
        ObjectAnimator.ofFloat(mDigitalClock, "alpha", alpha)
                .setDuration(500)
                .start();
        ObjectAnimator.ofFloat(mIndicator, "alpha", alpha)
                .setDuration(500)
                .start();
    }

    private void registerHomeKeyListener() {
        mHomeKeyReceiver = new HomeKeyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(mHomeKeyReceiver, filter);
    }

    private void unRegisterHomeKeyListener() {
        if (mHomeKeyReceiver != null) {
            unregisterReceiver(mHomeKeyReceiver);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMoveHandler.removeCallbacks(mMoveSaverRunnable);
    }

    public void setTimeFormat(TextClock clock, int amPmFontSize) {
        if (clock != null) {
            clock.setFormat12Hour(get12ModeFormat(amPmFontSize));
            clock.setFormat24Hour(get24ModeFormat());
        }
    }

    public CharSequence get12ModeFormat(int amPmFontSize) {
        String skeleton = "hma";
        String pattern = DateFormat.getBestDateTimePattern(Locale.getDefault(), skeleton);
        if (amPmFontSize <= 0) {
            pattern.replaceAll("a", "").trim();
        }
        pattern = pattern.replaceAll(" ", "\u200A");
        int amPmPos = pattern.indexOf('a');
        if (amPmPos == -1) {
            return pattern;
        }
        Spannable sp = new SpannableString(pattern);
        sp.setSpan(new StyleSpan(Typeface.NORMAL), amPmPos, amPmPos + 1, Spannable.SPAN_POINT_MARK);
        sp.setSpan(new AbsoluteSizeSpan(amPmFontSize), amPmPos, amPmPos + 1,
                Spannable.SPAN_POINT_MARK);
        sp.setSpan(new TypefaceSpan("sans-serif"), amPmPos, amPmPos + 1, Spannable.SPAN_POINT_MARK);
        return sp;
    }

    public void updateDate(TextView date) {
        String dateFormat = this.getString(R.string.abbrev_wday_month_day_no_year);
        Date now = new Date();
        final Locale l = Locale.getDefault();
        String fmt = DateFormat.getBestDateTimePattern(l, dateFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(fmt, l);
        date.setText(sdf.format(now));
    }

    public CharSequence get24ModeFormat() {
        String skeleton = "Hm";
        return DateFormat.getBestDateTimePattern(Locale.getDefault(), skeleton);
    }

    @Override
    protected void onDestroy() {
        mActivity = null;
        mHandler.removeMessages(COMMAND_CHANGE);
        mWakeLock.release();
        unRegisterHomeKeyListener();
        mSensorManager.unregisterListener(mLigthSensorListener, mLigthSensor);
        unregisterReceiver(BatteryBroadcastReceiver);
        super.onDestroy();
    }

    public static void finishActivity() {
        if (mActivity != null) {
            mActivity.finish();
        }
    }

    public static void startChargingClock(Context context) {
        if (mActivity == null || mActivity != null && mActivity.isDestroyed()) {
            Intent i = new Intent();
            i.setClass(context, ChargingClockActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    private class HomeKeyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }
            if (TextUtils.equals(intent.getAction(), Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra("reason");
                if (TextUtils.equals(reason, "homekey")) {
                    finishActivity();
                }
            }
        }

    }

    public static class ScreensaverMoveSaverRunnable implements Runnable {
        static final long MOVE_DELAY = 60000; // DeskClock.SCREEN_SAVER_MOVE_DELAY;
        static final long SLIDE_TIME = 10000;
        static final long FADE_TIME = 3000;

        static final boolean SLIDE = false;

        private View mContentView, mSaverView;
        private final Handler mHandler;

        private static TimeInterpolator mSlowStartWithBrakes;

        public ScreensaverMoveSaverRunnable(Handler handler) {
            mHandler = handler;
            mSlowStartWithBrakes = new TimeInterpolator() {
                @Override
                public float getInterpolation(float x) {
                    return (float) (Math.cos((Math.pow(x, 3) + 1) * Math.PI) / 2.0f) + 0.5f;
                }
            };
        }

        public void registerViews(View contentView, View saverView) {
            mContentView = contentView;
            mSaverView = saverView;
        }

        @Override
        public void run() {
            long delay = MOVE_DELAY;
            if (mContentView == null || mSaverView == null) {
                mHandler.removeCallbacks(this);
                mHandler.postDelayed(this, delay);
                return;
            }

            final float xrange = mContentView.getWidth() - mSaverView.getWidth();
            final float yrange = mContentView.getHeight() - mSaverView.getHeight();

            if (xrange == 0 && yrange == 0) {
                delay = 500; // back in a split second
            } else {
                final int nextx = (int) (Math.random() * xrange);
                final int nexty = (int) (Math.random() * yrange);

                if (mSaverView.getAlpha() == 0f) {
                    // jump right there
                    mSaverView.setX(nextx);
                    mSaverView.setY(nexty);
                    ObjectAnimator.ofFloat(mSaverView, "alpha", 0f, 1f)
                            .setDuration(FADE_TIME)
                            .start();
                } else {
                    AnimatorSet s = new AnimatorSet();
                    Animator xMove = ObjectAnimator.ofFloat(mSaverView,
                            "x", mSaverView.getX(), nextx);
                    Animator yMove = ObjectAnimator.ofFloat(mSaverView,
                            "y", mSaverView.getY(), nexty);

                    Animator xShrink = ObjectAnimator.ofFloat(mSaverView, "scaleX", 1f, 0.85f);
                    Animator xGrow = ObjectAnimator.ofFloat(mSaverView, "scaleX", 0.85f, 1f);

                    Animator yShrink = ObjectAnimator.ofFloat(mSaverView, "scaleY", 1f, 0.85f);
                    Animator yGrow = ObjectAnimator.ofFloat(mSaverView, "scaleY", 0.85f, 1f);
                    AnimatorSet shrink = new AnimatorSet();
                    shrink.play(xShrink).with(yShrink);
                    AnimatorSet grow = new AnimatorSet();
                    grow.play(xGrow).with(yGrow);

                    Animator fadeout = ObjectAnimator.ofFloat(mSaverView, "alpha", 1f, 0f);
                    Animator fadein = ObjectAnimator.ofFloat(mSaverView, "alpha", 0f, 1f);

                    if (SLIDE) {
                        s.play(xMove).with(yMove);
                        s.setDuration(SLIDE_TIME);

                        s.play(shrink.setDuration(SLIDE_TIME / 2));
                        s.play(grow.setDuration(SLIDE_TIME / 2)).after(shrink);
                        s.setInterpolator(mSlowStartWithBrakes);
                    } else {
                        AccelerateInterpolator accel = new AccelerateInterpolator();
                        DecelerateInterpolator decel = new DecelerateInterpolator();

                        shrink.setDuration(FADE_TIME).setInterpolator(accel);
                        fadeout.setDuration(FADE_TIME).setInterpolator(accel);
                        grow.setDuration(FADE_TIME).setInterpolator(decel);
                        fadein.setDuration(FADE_TIME).setInterpolator(decel);
                        s.play(shrink);
                        s.play(fadeout);
                        s.play(xMove.setDuration(0)).after(FADE_TIME);
                        s.play(yMove.setDuration(0)).after(FADE_TIME);
                        s.play(fadein).after(FADE_TIME);
                        s.play(grow).after(FADE_TIME);
                    }
                    s.start();
                }

                long now = System.currentTimeMillis();
                long adjust = (now % 60000);
                delay = delay
                        + (MOVE_DELAY - adjust) // minute aligned
                        - (SLIDE ? 0 : FADE_TIME) // start moving before the fade
                        ;
            }

            mHandler.removeCallbacks(this);
            mHandler.postDelayed(this, delay);
        }
    }
}
