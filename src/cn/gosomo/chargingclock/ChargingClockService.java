
package cn.gosomo.chargingclock;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class ChargingClockService extends Service {

    private static LocalHandler mHandler;
    private final static int COMMAND_STOP_SERVICE = 0;
    private final static int COMMAND_START_CLOCK = 1;
    private final static int COMMAND_DETECT_STATE = 2;
    private ScreenBroadcastReceiver mScreenReceiver;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mAgnetic;
    private boolean mIsInShowClockRange = false;
    private boolean mIsScreenOff = false;
    private final int COMMAND_DETECT_STATE_DELAY = 5000;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new LocalHandler();
        registerListener();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAgnetic = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorManager.registerListener(mFlipListener, mAccelerometer,
                SensorManager.SENSOR_DELAY_NORMAL, 300 * 1000);
        mSensorManager.registerListener(mFlipListener, mAgnetic,
                SensorManager.SENSOR_DELAY_NORMAL, 300 * 1000);
        mHandler.sendEmptyMessageDelayed(COMMAND_DETECT_STATE, COMMAND_DETECT_STATE_DELAY);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return Service.START_FLAG_RETRY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void registerListener() {
        mScreenReceiver = new ScreenBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(mScreenReceiver, filter);
    }

    @Override
    public void onDestroy() {
        unregisterListener();
        ChargingClockActivity.finishActivity();
        mSensorManager.unregisterListener(mFlipListener);
        super.onDestroy();
    }

    public void unregisterListener() {
        unregisterReceiver(mScreenReceiver);
    }

    private class ScreenBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                mIsScreenOff = true;
                if (mIsInShowClockRange) {
                    mHandler.sendEmptyMessage(COMMAND_START_CLOCK);
                }

            } else if (Intent.ACTION_SCREEN_ON.equals(action)) {
                mIsScreenOff = false;
            }
        }
    }

    public static void startService(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, ChargingClockService.class);
        context.startService(intent);
    }

    public static void stopService() {
        if (mHandler != null) {
            mHandler.removeMessages(COMMAND_STOP_SERVICE);
            mHandler.sendEmptyMessage(COMMAND_STOP_SERVICE);
        }
    }

    private class LocalHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case COMMAND_STOP_SERVICE:
                    stopSelf();
                    break;
                case COMMAND_START_CLOCK:
                    ChargingClockActivity.startChargingClock(getApplicationContext());
                    break;
                case COMMAND_DETECT_STATE:
                    if (isCharging(getApplicationContext())) {
                        if (mIsInShowClockRange) {
                            mHandler.sendEmptyMessage(COMMAND_START_CLOCK);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

    }

    private final SensorEventListener mFlipListener = new SensorEventListener() {
        private static final int FACE_UP_ANGLE_LIMIT = -75;
        private static final int FACE_UP_ANGLE_UPPER_LIMIT = -45;
        private float[] mAccelerometerValues = new float[3];
        private float[] mAgneticFieldValues = new float[3];

        @Override
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mAccelerometerValues = event.values;
            }
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                mAgneticFieldValues = event.values;
            }
            float[] values = new float[3];
            float[] R = new float[9];
            SensorManager.getRotationMatrix(R, null, mAccelerometerValues,
                    mAgneticFieldValues);
            SensorManager.getOrientation(R, values);
            values[1] = (float) Math.toDegrees(values[1]);
            values[2] = (float) Math.toDegrees(values[2]);
            float x = values[1];
            float y = values[2];
            if (isCharging(getApplicationContext())) {
                if (y > FACE_UP_ANGLE_LIMIT && y < FACE_UP_ANGLE_UPPER_LIMIT) {
                    mIsInShowClockRange = true;
                    if (mIsScreenOff) {
                        mHandler.sendEmptyMessageDelayed(COMMAND_START_CLOCK, 1000);
                    }
                } else {
                    mIsInShowClockRange = false;
                    ChargingClockActivity.finishActivity();
                }
            }
        }
    };

    private boolean isCharging(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        return status == BatteryManager.BATTERY_STATUS_CHARGING
                || status == BatteryManager.BATTERY_STATUS_FULL;
    }

}
