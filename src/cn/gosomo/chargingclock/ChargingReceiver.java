
package cn.gosomo.chargingclock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.text.TextUtils;

public class ChargingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String actionStr = intent.getAction();
        if (TextUtils.equals(actionStr, Intent.ACTION_BOOT_COMPLETED)
                || TextUtils.equals(actionStr, Intent.ACTION_POWER_CONNECTED)) {
            if (isCharging(context)) {
                ChargingClockService.startService(context);
            }
        } else if (TextUtils.equals(actionStr, Intent.ACTION_POWER_DISCONNECTED)) {
            ChargingClockService.stopService();
        }
    }

    private boolean isCharging(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean isAcCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        return (status == BatteryManager.BATTERY_STATUS_CHARGING
                || status == BatteryManager.BATTERY_STATUS_FULL) && isAcCharge;
    }

}
